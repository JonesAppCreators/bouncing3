﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

    public GameObject plane;
    public int columnWidth = 40;
    public int rowHeigtht = 20;
    public int width = 7;
    public int height = 5;
    bool created;
    public Transform target;

    public GameObject[,] grid = new GameObject[7, 5];

    void Awake()
    {
        for(int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject gridPlane = (GameObject)Instantiate(plane);
                gridPlane.transform.position = new Vector3(gridPlane.transform.position.x + x*columnWidth,
                    gridPlane.transform.position.y + y*rowHeigtht, gridPlane.transform.position.z);
                grid[x, y] = gridPlane;
            }
        }
    }

	// Use this for initialization
	void Start () {
        foreach(GameObject z in grid)
        {
            z.name = "not created";
        }

        grid[3, 0].name = "created";
        grid[3, 1].name = "created";

    }
	
	// Update is called once per frame
	void Update () {

	}

    public bool isCreated(GameObject grid)
    {
        return created;
    }

    public void setCreated()
    {
        created = true;
    }

    public int findCurrentColumn()
    {
        return (int)target.position.x / columnWidth;
    }

    public int findCurrentRow()
    {
        return (int)target.position.y / rowHeigtht;
    }

    public int findCenterX(int column)
    {
        // Find the center of the column
        return (column * columnWidth) + (columnWidth / 2);
    }

    public float findCenterY(int row)
    {
        return (row * rowHeigtht) + (rowHeigtht / 2);
    }

}


