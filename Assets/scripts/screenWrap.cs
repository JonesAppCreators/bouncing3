﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SpriteRenderer))]
public class screenWrap : MonoBehaviour {

    public bool wrapWidth = true;
    private Renderer _renderer;
    private Transform _transform;
    private Camera _camera;
    private Vector3 _viewportPosition;
    private bool isWrappingWidth;
    private Vector3 _newPosition;



	void Start ()
    {

        _renderer = GetComponent<Renderer>();
        _transform = transform;
        _camera = Camera.main;
        _viewportPosition = Vector3.zero;
        isWrappingWidth = false;
        _newPosition = _transform.position;
	}
	
	void LateUpdate ()
    {
        if(_transform.position.x <= 0 || _transform.position.x >= 280)
        {
            Wrap();
        }
	}

    private void Wrap()
    {
        bool isVisible = isBeingRendered();

        if (isVisible)
        {
            isWrappingWidth = false;
        }

        _newPosition = _transform.position;
        _viewportPosition = _camera.WorldToViewportPoint(_newPosition);

        if (wrapWidth)
        {
            if (!isWrappingWidth)
            {
                if(_viewportPosition.x > 1)
                {
                    _newPosition.x = _camera.ViewportToWorldPoint(Vector3.zero).x;
                    isWrappingWidth = true;
                }
                else if (_viewportPosition.x < 0)
                {
                    _newPosition.x = _camera.ViewportToWorldPoint(Vector3.one).x;
                    isWrappingWidth = true;
                }
            }
        }

        _transform.position = _newPosition;

    }

    private bool isBeingRendered()
    {
        if (_renderer.isVisible)
        {
            return true;
        }
        return false;
    }
}
