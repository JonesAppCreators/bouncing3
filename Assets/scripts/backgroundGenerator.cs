﻿using UnityEngine;
using System.Collections;

public class backgroundGenerator : MonoBehaviour
{

    public Grid _Grid;
    public skyGenerator _skyGenerator;
    public Randomcoingenerator _coinGenerator;
    public GameObject theBackground;
    public GameObject theSky;
    public float distanceBetween;
    public Transform generationPointRight;
    public Transform generationPointLeft;
    private float backgroundWidth;
    private float backgroundHeight;
    int currentColumn;
    int currentRow;


    void Start()
    {
        backgroundWidth = 40;
        backgroundHeight = 20;

        _coinGenerator.coinGeneratorOnStart();
    }

    void Update()
    {
        currentColumn = _Grid.findCurrentColumn();
        currentRow = _Grid.findCurrentRow();

        if (transform.position.x < generationPointRight.position.x)
        {
            if (currentColumn + 1 < _Grid.width)
            {
                if ((_Grid.grid[currentColumn + 1, currentRow]).name == "not created" )
                {
                    transform.position = new Vector3(_Grid.findCenterX(currentColumn) + backgroundWidth + distanceBetween, _Grid.findCenterY(currentRow), transform.position.z);
                    Instantiate(theBackground, transform.position, transform.rotation);
                    _coinGenerator.coinGenerator();
                    (_Grid.grid[currentColumn + 1, currentRow]).name = "created";
                }
            }
        }
        else if (transform.position.x > generationPointLeft.position.x)
        {
            if (currentColumn - 1 >= 0)
            {
                if ((_Grid.grid[currentColumn - 1, currentRow]).name == "not created")
                {
                    transform.position = new Vector3(_Grid.findCenterX(currentColumn) - backgroundWidth - distanceBetween, _Grid.findCenterY(currentRow), transform.position.z);
                    Instantiate(theBackground, transform.position, transform.rotation);
                    _coinGenerator.coinGeneratorLeft();
                    (_Grid.grid[_Grid.findCurrentColumn() - 1, _Grid.findCurrentRow()]).name = "created";
                }
            }
        }

        if (_skyGenerator.transform.position.y > generationPointRight.position.y)
        {
            if (currentRow + 1 < _Grid.height)
            {
                if ((_Grid.grid[currentColumn, currentRow + 1]).name == "not created")
                {
                    transform.position = new Vector3(_Grid.findCenterX(currentColumn), _Grid.findCenterY(currentRow) + backgroundHeight, transform.position.z);
                    Instantiate(theSky, transform.position, transform.rotation);
                    _coinGenerator.coinGeneratorSky();
                    (_Grid.grid[currentColumn, currentRow + 1]).name = "created";
                }
            }
        }
        if (_skyGenerator.transform.position.y < generationPointLeft.position.y)
        {
            if (currentRow - 1 >= 0 )
            {
                if ((_Grid.grid[currentColumn, currentRow - 1]).name == "not created")
                {
                    transform.position = new Vector3(_Grid.findCenterX(currentColumn), _Grid.findCenterY(currentRow) - backgroundHeight, transform.position.z);
                    Instantiate(theSky, transform.position, transform.rotation);
                    _coinGenerator.coinGeneratorSky();
                    (_Grid.grid[currentColumn, currentRow - 1]).name = "created";
                }
            }
        }
    }
}

