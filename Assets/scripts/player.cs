﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {

    private gameMaster gm;
    Grid _grid;

	// Use this for initialization
	void Start () {
        gm = GameObject.FindGameObjectWithTag("gameMaster").GetComponent<gameMaster>();
	}
	
	// Update is called once per frame
	void Update () {
         if (transform.position.x >= 280)
        {
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }
        if (transform.position.x <= 0)
        {
            transform.position = new Vector3(280, transform.position.y, transform.position.z);
        }

    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.CompareTag("coin")) {
            Destroy(col.gameObject);
            gm.points += 1;

        }
    }
}
