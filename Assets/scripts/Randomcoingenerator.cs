﻿using UnityEngine;
using System.Collections;

public class Randomcoingenerator : MonoBehaviour {

    public GameObject coin;
    public Transform generationPoint;
    public backgroundGenerator _backgroundGen;
    public Grid _Grid;
    public float distanceBetween;
    private float backgroundWidth;
    private float backgroundHeight;

    // Use this for initialization

    void Start () {

        backgroundWidth = 40;
        backgroundHeight = 20;
    }
	
	// Update is called once per frame
	void Update () {

    }


    public void coinGenerator()
    {
            for (int x = 1; x <= 30; x++)
            {
                Vector3 coinPos = new Vector3(Random.Range(_Grid.findCenterX(_Grid.findCurrentColumn()) + backgroundWidth - 19, _Grid.findCenterX(_Grid.findCurrentColumn()) + backgroundWidth + 19), Random.Range(_Grid.findCenterY(_Grid.findCurrentRow()) - 6, _Grid.findCenterY(_Grid.findCurrentRow()) + 9), -1);
                Instantiate(coin, coinPos, transform.rotation);
        }
    }
    public void coinGeneratorLeft()
    {
        for (int x = 1; x <= 30; x++)
        {
            Vector3 coinPos = new Vector3(Random.Range(_Grid.findCenterX(_Grid.findCurrentColumn()) - backgroundWidth - 19, _Grid.findCenterX(_Grid.findCurrentColumn()) - backgroundWidth + 19), Random.Range(_Grid.findCenterY(_Grid.findCurrentRow()) - 6, _Grid.findCenterY(_Grid.findCurrentRow()) + 9), -1);
            Instantiate(coin, coinPos, transform.rotation);
        }
    }

    public void coinGeneratorSky()
    {
        for (int x = 1; x <= 30; x++)
        {
            Vector3 coinPos = new Vector3(Random.Range(_Grid.findCenterX(_Grid.findCurrentColumn()) - 19, _Grid.findCenterX(_Grid.findCurrentColumn()) +  19), Random.Range(_Grid.findCenterY(_Grid.findCurrentRow()) + backgroundHeight - 7, _Grid.findCenterY(_Grid.findCurrentRow()) + backgroundHeight + 9), -1);
            Instantiate(coin, coinPos, transform.rotation);
        }
    }

    public void coinGeneratorOnStart() {
        for (int x = 1; x <= 30; x++)
        {
            Vector3 coinPos = new Vector3(Random.Range(_Grid.findCenterX(_Grid.findCurrentColumn()) - 19, _Grid.findCenterX(_Grid.findCurrentColumn()) + 19), Random.Range(4, 19), -1);
            Instantiate(coin, coinPos, transform.rotation);
        }
        for (int x = 1; x <= 30; x++)
        {
            Vector3 coinPos = new Vector3(Random.Range(_Grid.findCenterX(_Grid.findCurrentColumn()) - 19, _Grid.findCenterX(_Grid.findCurrentColumn()) + 19), Random.Range(20, 39), -1);
            Instantiate(coin, coinPos, transform.rotation);
        }
    }
}

