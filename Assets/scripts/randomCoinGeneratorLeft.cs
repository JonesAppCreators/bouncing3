﻿using UnityEngine;
using System.Collections;

public class randomCoinGeneratorLeft : MonoBehaviour {

    public GameObject coin;
    public Transform generationPoint;
    public float distanceBetween;
    private float backgroundWidth;

    // Use this for initialization
    void Start () {

        backgroundWidth = 40;

    }
	
	// Update is called once per frame
	void Update () {

        if (transform.position.x > generationPoint.position.x)
        {
            transform.position = new Vector3(transform.position.x - backgroundWidth - distanceBetween, transform.position.y, transform.position.z);
            for (int x = 1; x <= 30; x++)
            {
                Vector3 coinPos = new Vector3(Random.Range(transform.position.x - 19, transform.position.x + 19), Random.Range(0, 17), -1);
                Instantiate(coin, coinPos, transform.rotation);

            }
        }

    }
}
