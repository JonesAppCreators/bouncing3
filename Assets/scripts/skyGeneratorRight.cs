﻿using UnityEngine;
using System.Collections;

public class skyGeneratorRight : MonoBehaviour {

    public GameObject sky;
    public Transform generationPoint;
    public float distanceBetween;
    private float backgroundWidth;

    // Use this for initialization
    void Start () {
        backgroundWidth = 40;

    }
	
	// Update is called once per frame
	void Update () {
        while (transform.position.x < generationPoint.position.x)
        {
            Instantiate(sky, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + backgroundWidth, transform.position.y, transform.position.z);
        }
    }
}
