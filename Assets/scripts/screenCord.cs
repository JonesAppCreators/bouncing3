﻿using UnityEngine;
using System.Collections;
using System;

public class screenCord : MonoBehaviour {

    private int column;
    bool created;
    public GameObject rowList;
    public GameObject columnList;
    public GameObject levelList;
    
    public screenCord (int col)
    {
       column = col;
       created = false;
    }

    public bool isCreated()
    {
        return created;
    }

    public int getColumn()
    {
        return column;
    }
    
    public void setCreated()
    {
        created = true;
    }
}



public class screenRow
{
    public int row;
    public System.Collections.Generic.List<screenCord> columnList;

    public screenRow(int _row)
    {
        row = _row;
    }

    public void Add(screenCord coord)
    {
        columnList.Add(coord);
    }

    public screenCord Find(int col)
    {
        if (col < columnList.Count)
            return columnList[col - 1];
        else
            return null;
    }

    
}

public class Level{
    public System.Collections.Generic.List<screenRow> rowList;
    public System.Collections.Generic.List<screenCord> columnList;
    public int level, maxRight, maxHigh, currentColumn, currentRow;

    public Level(int _level, int _maxRight, int _maxHigh)
    {
        level = _level;
        maxRight = _maxRight;
        maxHigh = _maxHigh;
    }

    public void create()
    {
         screenRow row1;
         screenCord coord;

        for(int y = 1; y <= maxHigh; y++)
        {
            row1 = new screenRow(y);
            rowList.Add(row1);
            for (int x = 1; x <= maxRight; x++)
            {
                coord = new screenCord(x);
                columnList.Add(coord);
            }
        }
    }
    

    screenRow Find(int row)
    {
        if (row < rowList.Count)
            return rowList[row - 1];
        else
            return null;
    }


}
 
