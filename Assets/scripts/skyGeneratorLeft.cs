﻿using UnityEngine;
using System.Collections;

public class skyGeneratorLeft : MonoBehaviour {

    public GameObject sky;
    public Transform generationPoint;
    public float distanceBetween;
    private float backgroundWidth;

    // Use this for initialization
    void Start () {

        backgroundWidth = 40;
	
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x > generationPoint.position.x)
        {
            transform.position = new Vector3(transform.position.x - backgroundWidth, transform.position.y, transform.position.z);
            Instantiate(sky, transform.position, transform.rotation);
        }

    }
}
