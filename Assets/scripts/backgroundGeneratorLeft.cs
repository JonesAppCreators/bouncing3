﻿using UnityEngine;
using System.Collections;

public class backgroundGeneratorLeft : MonoBehaviour {

    public GameObject theBackground;
    public Transform generationPoint;
    public float distanceBetween;
    private float backgroundWidth;

    // Use this for initialization
    void Start () {
        backgroundWidth = 40;

    }
	
	// Update is called once per frame
	void Update() {
        if (transform.position.x > generationPoint.position.x)
        {
            transform.position = new Vector3(transform.position.x - backgroundWidth - distanceBetween, transform.position.y, transform.position.z);
            Instantiate(theBackground, transform.position, transform.rotation);
        }

    }
}
